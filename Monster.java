import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Panel;
import java.awt.Point;


public class Monster {

 double x, y;
 double vx, vy;
 double width, height;
 
 public Monster(double startX, double startY, int type, Panel theWorld)
 {
  
  switch (type) {
  case 1:
   this.x=startX;
   this.y=startY;
   vx=0.7;
   vy=0.7;
   width=10;
   height=30;
   break;

  default:
   break;
  }
 }
 
 public double getX()
 {
  return(x);
 }
 public double getY()
 {
  return(y);
 }
 public double getWidth()
 {
  return(width);
 }
 public double getHeight()
 {
  return(height);
 }
 

 
 public void move(Point playerPos)
 {
  if(playerPos.x<x){
   x = x - vx;
  } else if (playerPos.x>x){
   x = x + vx;
  }
  
 }
 public void gravity(Dimension S)
 {
  
    if(y < S.height*0.95)
    {
     vy = vy + 0.0982;
    
     y +=vy;
    }
    if( y > S.height*0.95)
    {
     y = S.height*0.95;
    }
    
  
 }

 public void draw(Graphics g){
  g.setColor(Color.red); 
  g.fillRect((int)(x), (int)(y-height), (int)width, (int)height);
 }
 
}