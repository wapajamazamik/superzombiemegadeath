public class MenuClass{
 
 Button START;
 Button OPTIONS;
 Button CREDITS;
 Button EXIT;
 
 Label name;
 
 Panel menuPanel;
 Panel backPanel;
 
 public MenuClass(Button START, Button OPTIONS, Button CREDITS, Button EXIT, Label name, Panel menuPanel, Panel backPanel){
  
  this.START=START;
  this.OPTIONS=OPTIONS;
  this.CREDITS=CREDITS;
  this.EXIT=EXIT;
  
  this.name= name;
  
  this.menuPanel=menuPanel;
  
 }

 public void menuInit(){
  
  Dimension S = backPanel.getSize();
  
  backPanel.setLayout(null);
  backPanel.setBackground(Color.BLACK);
  
  menuPanel.setBounds((int)(S.width/3),(int)(S.height*0.2),(int)(S.width/3),(int)(S.height*0.6));
  
  backPanel.add(menuPanel);
  name.setFont(new Font("Super zombie Mega Death", Font.BOLD, (int)(S.width*0.0265)));
  name.setForeground(Color.red);
  menuPanel.add(name);
  
  START.setFont(new Font("Start", Font.PLAIN, (int)(S.width*0.0265) ) );
  START.setBackground(Color.red);
  menuPanel.add(START);
  
  OPTIONS.setFont(new Font("Options", Font.PLAIN, (int)(S.width*0.02) ) );
  OPTIONS.setBackground(Color.red);
  menuPanel.add(OPTIONS);
  
  CREDITS.setFont(new Font("Credits", Font.PLAIN, (int)(S.width*0.02) ) );
  CREDITS.setBackground(Color.red);
  menuPanel.add(CREDITS);
  
  EXIT.setFont(new Font("EXIT", Font.PLAIN, (int)(S.width*0.02) ) );
  EXIT.setBackground(Color.red);
  menuPanel.add(EXIT);
  
 
 }
 
 public void menuAction(ActionEvent ae){
  if(ae.getSource()==START){
   
  } else if(ae.getSource()==OPTIONS){
   
  } else if(ae.getSource()==CREDITS){
   
  } else if(ae.getSource()==EXIT){
   System.exit(0);
  }
 }
 
 public void menuComRis(ComponentEvent e ){
  Dimension S = backPanel.getSize();
  
  name.setFont(new Font("Super zombie Mega Death", Font.BOLD, (int)(S.width*0.0265)));
  START.setFont(new Font("Start", Font.PLAIN, (int)(S.width*0.0265) ) );
  OPTIONS.setFont(new Font("Options", Font.PLAIN, (int)(S.width*0.02) ) );
  CREDITS.setFont(new Font("Credits", Font.PLAIN, (int)(S.width*0.02) ) );
  EXIT.setFont(new Font("EXIT", Font.PLAIN, (int)(S.width*0.02) ) );
  
  menuPanel.setBounds((int)(S.width/3),(int)(S.height*0.2),(int)(S.width/3),(int)(S.height*0.6));
 }

 
}