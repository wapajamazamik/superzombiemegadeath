import java.awt.*;
public class Box {
	 double x;
	 double y;
	private double w;
	private double h;
	private int health;
	double vy;
	double vx;
	
	//Debree
	double x2;
	double y2;
	double y3;
	double vx2;
	double vy2;
	
	public Box(Panel theWorld)
	{
		
		this.x = 100;
		this.y = 100;
		this.w = 30;
		this.h = 30;
		health = 30;
		
		this.y2  =y;
		this.y3  =y;
		this.x2  =x;
		this.vx2 = 1;
		this.vy2 = -10;
	}
	
	public void draw(Graphics g)
	{	
		
		
		if(health <= 0)
		{	
			g.setColor(Color.black);
			
			//Test of debree
			g.fillRect((int)(x),    (int)(y2), (int)(w/2), (int)(h/2));//upper left
			g.fillRect((int)(x2+w/2),(int)(y2), (int)(w/2),(int)(h/2));//upper right
			
			g.fillRect((int)(x),	(int)(y3+h/2+vy2), (int)(w/2), (int)(h/2));//lower left
			g.fillRect((int)(x2+w/2),(int)(y3+h/2+vy2), (int)(w/2), (int)(h/2));//lower right
			
			vy2= vy2+1;
			
			y2 += vy2;
			x -= vx2;
			x2 +=vx2;
			
			y3 += vy2+5;
			
		}else{
			g.setColor(Color.green);
		}
		
		if(health > 0)
		{
			g.fillRect((int)x, (int)y, (int)w, (int)h);
			this.y2  =y;
			this.x2  =x;
			this.y3  =y;
		}
	}
	
	//Manuell inst�llning av v�rden i l�dan
	public void setX(double x){	this.x=x;}
	public void setY(double y){	this.y=y;}
	public void setW(double w){	this.w=w;}
	public void setH(double h){	this.h=h;}
	public void setHealth(int health){	this.health=health;}
	public void setVx(double vx){	this.vx=vx;}
	public void setVy(double vy){	this.vy=vy;}
	
	//Physics
	public boolean doesCollide(int x,int y)
	{
		if(x < this.x || x > this.x+w)return false;
		else if(y < this.y || y > this.y+h)return false; 
		else 
		{
			return true;
		}
	}
	
	
	public void gravity(Dimension S)
	{
		
		if(y < S.height*0.9)
		{
			vy = vy + 0.25;
			y +=vy;
		}
		if( y > S.height*0.9)
		{
			y = S.height*0.9;
		}
		
	}
	
	//Combat
	public void takeDmg(int dmg){ health = health - dmg;}
	
	public boolean hasHealth()
	{
		if(health <= 0)
		{
			return false;
		}
		else
		{
			return true;	
		}
	}
	
}
