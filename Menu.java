import java.applet.Applet;
import java.awt.Button;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;


public class Menu extends Applet implements ComponentListener, ActionListener {

 Button START = new Button("Start");
 Button OPTIONS = new Button("Options");
 Button CREDITS = new Button("Credits");
 Button EXIT = new Button("Exit");
 
 Panel menuPanel = new Panel(new GridLayout(5,1));
 
 Label name = new Label("Super Zombie Mega Death");
 
 MenuClass Menu = new MenuClass(START, OPTIONS, CREDITS, EXIT, name, menuPanel, this);
 
 public void init(){

  Menu.menuInit();
  
  addComponentListener(this);
  START.addActionListener(this);
  OPTIONS.addActionListener(this);
  CREDITS.addActionListener(this);
  EXIT.addActionListener(this);
 }
 
 
 public void actionPerformed(ActionEvent ae){
  Menu.menuAction(ae);
 }
 
 public void componentHidden(ComponentEvent e) {
  
 }
 
 public void componentMoved(ComponentEvent e) {
  
 }

 public void componentResized(ComponentEvent e) {
  Dimension S = getSize();
  Menu.menuComRis(e);
 }

 public void componentShown(ComponentEvent e) {
  
  
 }


 
 
}