import java.awt.*;
public class Player {
	//Player
	private int width,height;
	private double x,y, vx ,vy;
	private int WIDTH, HEIGHT;
	private int health;
	//Experiment :3
	private boolean collide;
	
	//Lifebar
	private int LBGreenX;
	private int LBGreenY;
	private int LBGreenW;
	private int LBGreenH;
	private int LBRedX;
	private int LBRedY;
	private int LBRedW;
	private int LBRedH;
	
	public Player (Panel theWorld)
	{
		//Player bottom
		Dimension size = theWorld.getSize();
		WIDTH = size.width;
		HEIGHT = size.height;
		
		x = WIDTH/2;
		y = HEIGHT/2;
		
		//Player props
		width = 10;
		height = 40;
		vx = 3;
		vy = 0;
		health = 100; 
		
		
		//LBInit
		
		LBGreenX = 20;
		LBGreenY = 20;
		LBGreenW = health;
		LBGreenH = 20;
		LBRedX = 20-LBGreenW;
		LBRedY = 20;
		LBRedW = 100-health;
		LBRedH = 20;
		
		
		
	}
	
	public void draw(Graphics g)
	{
		//LifeBar has to be here in order to change with health
		LBGreenX = 20;
		LBGreenY = 20;
		LBGreenW = health;
		LBGreenH = 20;
		LBRedX = 20+LBGreenW;
		LBRedY = 20;
		LBRedW = 100-health;
		LBRedH = 20;
		
		//Paint
		g.setColor(Color.black);
		g.fillRect((int)x,(int)y,width, height);
		
		//LifeBar green
		g.setColor(Color.green);
		g.fillRect(LBGreenX, LBGreenY, LBGreenW,LBGreenH);
		
		//LifeBar red
		g.setColor(Color.red);
		g.fillRect(LBRedX, LBRedY, LBRedW, LBRedH);
	}
	
	
	public int getX()
	{
		return (int)x;
	}
	
	public int getY()
	{
		return (int)y;
	}
	
	public void moveRight()
	{
			x += vx;
	}
	
	public void moveLeft()
	{
				x -= vx;
	}
	
	public void gravity(Dimension S)
	{
		
		if(y < S.height*0.9)
		{
			vy = vy + 0.25;
			y +=vy;
		}
		if( y > S.height*0.9)
		{
			y = S.height*0.9;
		}
		
	}
	public void setVy(int vy, Dimension S)
	{
		
		if(y ==  S.height*0.9){
			this.vy = vy;
			y--;
			}
	}
	
	public boolean doesHit(Box l�da)
	{
		int PlayerCX = (int)(this.x + width/2);
		int PlayerCY = (int)(this.y + height/2);
		
		
		for(int tx = PlayerCX + width/2+1; tx < PlayerCX + width/2 + 10; tx++)
		{
			collide = l�da.doesCollide(tx, PlayerCY);
			if(collide)
			{
				tx = tx + 100;
			}
			
		}
		
		if(collide)
			{
				
				return true;
			}
			else
			{
				return false;
			}
	}
	
	public void takeDmg(int dmg)
	{
		if(health != 0 )
		{
			health = health - dmg;
		}
		
		if(dmg > health)health = 0;
	}
}
