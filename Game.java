import java.applet.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;

public class Game extends Applet implements Runnable , KeyListener{

	
//	Vector boxes = new Vector();
	Player Spelare = new Player(this);
	Monster zom = new Monster(50,50,0,this);
	Box[] boxes = new Box[10];
	//Tillf�llig test av vektor
	
	
	//buffer
	Graphics rityta;
	Image minnesbild;
	boolean moveRight = false;
	boolean moveLeft = false;
	boolean leftDown = false, rightDown = false; 
	
	public void init()
	{
		//boxes.addElement(L�DA);
		addKeyListener(this);
		requestFocus();
		for(int n = 0; n < boxes.length; n++)
		{
			boxes[n] = new Box(this);
			boxes[n].setX(100+n*60);
		}
	}

	public void paint(Graphics g)
	{
		g.setColor(Color.black);
		Spelare.draw(g);
		zom.draw(g);
		for(int n = 0; n < boxes.length; n++)
		{
			boxes[n].draw(g);
			
		}
		//g.fillRect(Spelare.getX(), Spelare.getY(), 10, 30);
	
	}
	
	public void start()
	{
	
		Thread t = new Thread(this);
		t.start();
		
	}
	
	public void run()
	{
	
		while(true){
			Dimension S = getSize();
			
			
			if(moveRight)
			{
				Spelare.moveRight();
			}
			if(moveLeft)
			{
				Spelare.moveLeft();
			}
			Spelare.gravity(S);
			for(int n = 0 ; n < boxes.length; n++)
			{
				boxes[n].gravity(S);
				
			}
			minnesbild = createImage(S.width, S.height);
			rityta = minnesbild.getGraphics();
			repaint();
			
			try{
				Thread.sleep(10);
				}catch(InterruptedException ie){}		
			}
	}
	
	public void update(Graphics g)
	{
		Dimension S = getSize();	
		rityta.clearRect(0,0,S.width,S.height);
		
		paint(rityta);
		g.drawImage(minnesbild, 0,0, this);
	}
	
	
	public void keyPressed(KeyEvent ke)
	{
		Dimension S = getSize();
		if(ke.getKeyCode()==KeyEvent.VK_UP) Spelare.setVy(-5, S);
		if(ke.getKeyCode()==KeyEvent.VK_LEFT) moveLeft = true;
		if(ke.getKeyCode()==KeyEvent.VK_RIGHT) moveRight = true;
		if(ke.getKeyCode()==KeyEvent.VK_A) 
		{
			int boxTarget = 0;
			boolean hit = false;
			
			for(int n = 0; n<boxes.length; n++)
			{
				hit = Spelare.doesHit(boxes[n]);
				
				if(hit)
				{
					boxTarget = n;
					n = boxes.length;
				}
			}
			
			if(hit)
			{
				boxes[boxTarget].takeDmg(10);
			}
		}
		if(ke.getKeyCode()==KeyEvent.VK_K)
		{
			Spelare.takeDmg(10);
		}
		
	}
	
	public void keyReleased(KeyEvent ke)
	{
		if(ke.getKeyCode()==KeyEvent.VK_LEFT) moveLeft = false;
		if(ke.getKeyCode()==KeyEvent.VK_RIGHT) moveRight = false;
	}
	
	public void keyTyped(KeyEvent ke){}
	
}
